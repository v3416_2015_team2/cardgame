#include "gameinfo.h"

GameInfo::GameInfo()
{
    serverCon = new NetConnect(0,this);
    position = 0;
    for (int i=0; i < 30; i++)
        boolForBloc[i]= false;

    firstType = NULL;
    seconType = NULL;
    firPlayer = new PlayerInfo(1);
    secPlayer = new PlayerInfo(2);
    ternNum = 1;//2
    for (int i =1; i < 16; i++)
    {
        typeVec.push_back(i);
        typeVec.push_back(i);
    }
    serverCon->connectTcp();

}

int GameInfo::GetCardType()
{
    int sizen = typeVec.size();

    int t = (rand()%30);
    int start = t;
    while (typeVec[t] == -1)
    {
        t++;
        if (t == sizen)
        {
            t=0;
        }
        if (start == t)
        {
            break;
        }
    }
    typeOfCards[position] = typeVec[t];
    position++;
    sizen = typeVec[t];
    typeVec[t] = -1;
    //qDebug() << "Set Type" << sizen;
    return sizen;
}

int GameInfo::GetChangeCardType()
{
    //if (position == 29)
    //    position = -1;
    position++;
    qDebug() << typeOfCards[position];
    return typeOfCards[position];
}

bool GameInfo::IsHit()
{
   if (firstType == seconType)
       return true;
   return false;
}

void GameInfo::GetMassage(QByteArray data)
{
    //blockGameCards();
    QByteArray tes = "";
    tes = tes +data[0]+data[1]+data[2];
    QString stes (tes) ;
    int tN = stes.toInt();
    qDebug() << data;
    //qDebug() << stes;
    //qDebug() << tN;
    switch (tN)
    {
    case 000:
    {
        tes = "";
        qDebug() <<"Command 0";
        for (int i =29; i >= 0; i--)
        {
            tes = tes + "_" + QByteArray::number(typeOfCards[i]) ;
        }
        serverCon->writeComeData(tes);
        ternNum = 1;
        break;
    }
    case 001:
    {
        qDebug() <<"Command 1";
        int tens=0;
        int posNum = 0;
        int inputNum =0;
        for (int i =4; i < data.size(); i++)
        {
            //qDebug () << "InputData " << (int)data[i];
            if ((int)data[i] == 95)
            {

                typeOfCards[posNum] = inputNum;
                posNum++;
                tens = 0;
                inputNum = 0;
                qDebug() << "+"<< typeOfCards[posNum-1];
            }
            else
            {
                inputNum=inputNum* pow(10.0,tens) + (data[i] - 48) ;
                tens++;
            }
        }
        typeOfCards[posNum] = inputNum;
        qDebug() << "+"<< typeOfCards[posNum];
        qDebug() <<"Change Commited";
        position = -1;
        ternNum = 0;
        blockGameCards();
        fieldTextChange();
        changeCardTypeGame();
        break;
    }
    case 002:
    {
        qDebug() <<"Command 2";
        int tens=0;
        int posNum = 0;
        int inputNum =0;
        int startPos=4;
        while ((int)data[startPos] != 95)
        {
            inputNum=inputNum* pow(10.0,tens) + (data[startPos] - 48) ;
            tens++;
            startPos++;
        }
        startPos++;
        setPlTwoScore(inputNum);
        qDebug ()<< "SetScore " << inputNum;
        inputNum =0;
        tens=0;
        for (int i =startPos; i < data.size(); i++)
        {
            //qDebug () << "InputData " << (int)data[i];
            if ((int)data[i] != 95)
            {
                boolForBloc[posNum] = (data[i] - 48);
                posNum++;
                qDebug() << "*"<< boolForBloc[posNum-1];
            }

        }
        ternNum = 0;
        //unblockGameCards();
        fieldTextChange();
        openGameBlockedCards();
        break;
    }
    case 003:
    {
        qDebug() <<"Command 3";
        int tens=0;
        int posNum = 0;
        int inputNum =0;
        int startPos=4;
        while ((int)data[startPos] != 95)
        {
            inputNum=inputNum* pow(10.0,tens) + (data[startPos] - 48) ;
            tens++;
            startPos++;
        }
        startPos++;
        setPlTwoScore(inputNum);
        qDebug ()<< "SetScore " << inputNum;
        inputNum =0;
        tens=0;
        for (int i =startPos; i < data.size(); i++)
        {
            //qDebug () << "InputData " << (int)data[i];
            if ((int)data[i] != 95)
            {
                boolForBloc[posNum] = (data[i] - 48);
                posNum++;
                qDebug() << "*"<< boolForBloc[posNum-1];
            }

        }
        ternNum = 1;
        unblockGameCards();
        fieldTextChange();
        openGameBlockedCards();
    }
    default:
    {
        qDebug() << "Unknow Command";
    }
    }

}



bool GameInfo::IsEndOfGame()
{
    qDebug()<<"--" << firPlayer->scorN();
    for (int i =0; i < 30; i ++)
    {
        if (!boolForBloc[i])
            return false;
    }
    return true;
}

void GameInfo::BlockCard(int n)
{
    boolForBloc[n] = true;
}

void GameInfo::Sleep()
{
    QTime time;
    time.start();
    for(;time.elapsed() < 1000;)
    {

    }
}

void GameInfo::SendDataToServerEnd()
{
    QByteArray tes="";
    //for (int i =29; i >= 0; i--)
    tes = tes + "_"+ QByteArray::number(plOneScore());
    for (int i =0; i < 30; i ++)
    {
        tes = tes + "_" + QByteArray::number(boolForBloc[i]) ;
    }
    qDebug () << tes;

    serverCon->writeComeData("003"+tes);
}

void GameInfo::SendDataToServer()
{
    QByteArray tes="";
    //for (int i =29; i >= 0; i--)
    tes = tes + "_"+ QByteArray::number(plOneScore());
    for (int i =0; i < 30; i ++)
    {
        tes = tes + "_" + QByteArray::number(boolForBloc[i]) ;
    }
    qDebug () << tes;

    serverCon->writeComeData("002"+tes);
}

int GameInfo::numN_1()
{
    return firstNumb;
}

void GameInfo::setNumN_1(int t)
{
    firstNumb = t;
}

int GameInfo::numN_2()
{
    return seconNumb;
}

void GameInfo::setNumN_2(int t)
{
    seconNumb = t;
}

bool GameInfo::ternN()
{
    return ternNum;
}

void GameInfo::setTernN(bool t)
{
    ternNum = t;
}

int GameInfo::plOneScore()
{
    return firPlayer->scorN();
}

void GameInfo::setPlOneScore(int n)
{
    firPlayer->setScorN(n);
}

int GameInfo::plTwoScore()
{
    return secPlayer->scorN();
}

void GameInfo::setPlTwoScore(int n)
{
    secPlayer->setScorN(n);
}

int GameInfo::WhoWins()
{
    qDebug() << plOneScore()<<"_____"<<plTwoScore();
    if (plOneScore() == plTwoScore())
        return 0;
    else
    {
        if (firPlayer->scorN() > secPlayer->scorN())
            return 1;
        else
            return 2;
    }
}

bool GameInfo::PermisionForFlip(int num)
{
    return boolForBloc[num];
}

bool GameInfo::WaitSecCard()
{
    if (seconType==NULL)
    {
        return false;
    }   
    return true;
}

void GameInfo::GetCard(int type,int num)
{

    if (firstType == NULL)
    {
        firstNumb = num;
        firstType = type;
    }
    else
    {
        seconNumb = num;
        seconType = type;
        if (IsHit())
        {
            qDebug()<<"BINGO";
        }
        else
        {
          qDebug()<<"Sorry,NOT";
          ternNum=!ternNum;
        }
        readTwoNumbs();
        firstType = NULL;
        seconType = NULL;
    }
}

void GameInfo::IncreseScore()
{

    firPlayer->addScore();
}

void GameInfo::IncreseScore1()
{
    secPlayer->addScore();
}

