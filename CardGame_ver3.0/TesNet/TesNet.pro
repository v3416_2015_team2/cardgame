#-------------------------------------------------
#
# Project created by QtCreator 2015-11-08T07:53:17
#
#-------------------------------------------------

QT       += core gui
QT       += core network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TesNet
TEMPLATE = app


SOURCES += main.cpp\
    mserver.cpp

HEADERS  += \
    mserver.h

CONFIG += mobility
MOBILITY = 

