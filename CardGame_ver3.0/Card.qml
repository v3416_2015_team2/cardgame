import QtQuick 2.4
import Cards 1.0

Flipable
{
    id: flipable
    width: 70
    height: 90

    //property bool flipped: true
    property bool flipped: false
    property bool flippedAble: true;//false;//true
    property int cardNomer
    property int cardType
    signal stopFlippe
    signal runFlippe
    signal closeCard
    signal refreshCard
    signal openCard

    Connections {
        target: flipable
        onStopFlippe: {
            flippedAble = false
        }
    }
    Connections {
        target: flipable
        onRunFlippe: {
            flippedAble = true
        }
    }
    Connections {
        target: flipable
        onCloseCard: {
            flipped = false
            flippedAble = true
        }
    }
    Connections {
        target: flipable
        onRefreshCard: {
            backImage.source = cardInf.GetCardBack()
            cardInf.type = cardType;
            //update()
        }
    }
    Connections {
        target: flipable
        onOpenCard:{
            flipped = true
        }
    }

    Card_Inf
    {
        id: cardInf
        numN: cardNomer
        type: cardType
    }
    front:
        Image{
        id:frontImage
        source: cardInf.GetTopName()
    }
    back:
        Image{
        id:backImage
        source: cardInf.GetCardBack()
    }
    transform:
    Rotation
    {
        id: rotation

        origin.x: flipable.width/2
        origin.y: flipable.height/2

        axis.x: 0; axis.y:1; axis.z: 0
        angle: 0
    }
    states:
    State
    {
        name: "Back"
        PropertyChanges { target: rotation; angle: 180 }
        when: flipable.flipped
    }

    transitions:
    Transition
    {
        NumberAnimation
        {
            target: rotation; property: "angle"; duration: 1000
        }
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: {
        if (flippedAble)
        {
            console.log("\n\n")
            console.log("CardNum "+ cardInf.numN)
            console.log("TypeNum "+ cardInf.type)
            flipable.flipped = !flipable.flipped
            stopFlippe()
            game_info.GetCard(cardInf.type,cardInf.numN)
        }
        else
        {
            console.log("\n\nBlocked Card")
            //flippedAble = true
        }


        }
    }

}
