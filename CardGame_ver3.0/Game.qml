import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0

Item
{
    id: game

    //width: 480
    //height: 800
    property int numFir
    property int numSec
    property int sound: 0
    property bool twoCardsCheck:false
    signal exitToMenu
    signal getFirNum
    signal closeChoseCards
    signal endOfTerm
    signal changeCardTypeGame

    Connections {
        target: game
        onCloseChoseCards: {
            field.blockCards()
            twoCardsCheck=true
        }
    }

    Connections {
        target: game
        onEndOfTerm: {
                if (twoCardsCheck)
                {
                    twoCardsCheck=false
                    if (game_info.ternN)
                    {
                        console.log("Cards Was Blocked")
                        game_info.BlockCard(game.numFir)
                        game_info.BlockCard(game.numSec)
                        game_info.IncreseScore()
                        field.unblockCards()
                        game_info.SendDataToServer();
                    }
                    else
                    {
                        field.closeTwoCards()
                        field.blockCards()
                        game_info.SendDataToServerEnd();


                    }
                    field.switchText()


                }
            }

    }

    Game_Inf
    {
        id: game_info
        Connections {
            target: game_info
            onReadTwoNumbs: {
                game.numFir = game_info.numN_1
                game.numSec = game_info.numN_2
                //PauseAnimation { duration: 100 }
                closeChoseCards()
            }
        }
        Connections {
            target: game_info
            onBlockGameCards: {
                //console.log("EEEEEEEE")
                field.blockCards()
            }
        }
        Connections {
            target: game_info
            onUnblockGameCards: {
                //console.log("EEEEEEEE")
                field.unblockCards()
            }
        }
        Connections {
            target: game_info
            onChangeCardTypeGame: {

                field.changeCardType()
            }
        }
        Connections {
            target: game_info
            onFieldTextChange: {

                field.switchText()
            }
        }

        Connections {
            target: game_info
            onOpenGameBlockedCards: {

                field.openBlockedCards()
            }
        }

    }

    Field
    {
        id: field
    }




}

