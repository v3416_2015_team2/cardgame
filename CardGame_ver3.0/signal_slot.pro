TEMPLATE = app

QT += qml quick gui core declarative network

TARGET = example
TEMPLATE = app

SOURCES += main.cpp \
    receiver.cpp \
    cardinfo.cpp \
    gameinfo.cpp \
    playerinfo.cpp \
    netconnect.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    receiver.h \
    cardinfo.h \
    gameinfo.h \
    playerinfo.h \
    netconnect.h
