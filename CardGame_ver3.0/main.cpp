#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <time.h>
//#include "receiver.h"


#include "gameinfo.h"
#include "cardinfo.h"





int main(int argc, char *argv[])
{
    srand (time(NULL));
    QGuiApplication app(argc, argv);
    //qmlRegisterType<Card_class>("Shapes",1,0,"QMLCard");TriangleItem
    //qmlRegisterType<TriangleItem>("Shapes",1,0,"QMLCard");
    qmlRegisterType<CardInfo>("Cards",1,0,"Card_Inf");
    qmlRegisterType<GameInfo>("Cards",1,0,"Game_Inf");
    qmlRegisterType<PlayerInfo>("Cards",1,0,"Player_Inf");
    QQmlApplicationEngine engiMne;


//    Receiver receiver;

//    QQmlContext* ctx = engiMne.rootContext();
//    ctx->setContextProperty("receiver", &receiver);
    engiMne.load(QUrl(QStringLiteral("qrc:///main.qml")));
    return app.exec();
}
