#ifndef GAMEINFO_H
#define GAMEINFO_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <random>
#include <QTime>
#include <vector>
#include "playerinfo.h"
using namespace std;
class GameInfo : public QQuickItem
{
    Q_OBJECT
protected:
    Q_PROPERTY(int numN_1 READ numN_1 WRITE setNumN_1 NOTIFY numN_1Changed)
    Q_PROPERTY(int numN_2 READ numN_2 WRITE setNumN_2 NOTIFY numN_2Changed)
    Q_PROPERTY(bool ternN READ ternN WRITE setTernN NOTIFY ternNChanged)
    vector<int> typeVec;
    bool ternNum;
    PlayerInfo *firPlayer;
    PlayerInfo *secPlayer;
    int firstType;
    int seconType;
    int firstNumb;
    int seconNumb;
    bool boolForBloc[30] ;
public:
    GameInfo();
    bool IsHit();


    int numN_1();
    void setNumN_1(int t);
    int numN_2();
    void setNumN_2(int t);
    bool ternN();
    void setTernN(bool t);

signals:
    void numN_1Changed();
    void numN_2Changed();
    void ternNChanged();
    void readTwoNumbs();
    void switchPlayer();

public slots:
    int WhoWins();
    bool IsEndOfGame();
    void BlockCard(int n);
    bool PermisionForFlip(int num);
    int GetCardType();
    bool WaitSecCard();
    void GetCard(int type,int num);
    void Sleep();
};

#endif // GAMEINFO_H
