import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
Item {
    id: disMes
    Text {
        id: textMes
        text: qsTr("You Were Disconnect from Server")
        y:0
        x:parent.width/2-15
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            disMes.visible = false;
            mainMenu.visible = true;
        }
    }

}

