import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

Item
{
    id: optionsMenu

    property int sound: 0
    property int minSound: 0
    property int maxSound: 15

    signal done

    onVisibleChanged:
    {
        soundSlider.setValue(sound);
    }

    GridLayout
    {
        anchors.fill: parent
        columns: 2
        columnSpacing: 30
        rowSpacing: 30

        Text
        {
            text: qsTr("Game options")
            font
            {
                family: "Verdana"
                pointSize: 15
            }

            Layout.alignment: Qt.AlignHCenter
            Layout.columnSpan: 2
        }

        Label
        {
            text: qsTr("Sound settings")

            Layout.alignment: Qt.AlignTop
        }

        Qml_SignedSlider
        {
            id: soundSlider

            minValue: minSound
            maxValue: maxSound
            initValue: sound

            Layout.fillWidth: true
        }

        Item
        {
            Layout.fillHeight: true
            Layout.columnSpan: 2
        }

        Qml_MyButton
        {
            text: qsTr("Save and back to main menu")

            Layout.fillWidth: true
            Layout.columnSpan: 2

            onClicked:
            {
                sound = soundSlider.value;
                done();
            }
        }

        Qml_MyButton
        {
            text: qsTr("Back to main menu")

            Layout.fillWidth: true
            Layout.columnSpan: 2

            onClicked: done()
        }
    }
}


