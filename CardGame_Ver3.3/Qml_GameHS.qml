import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0

Item
{
    id: gameHS
    property int sound: 0
    property bool prevTern: true;
    property bool twoCardsCheck:false

    signal gameStartConnect
    signal gameExitToMenu
    signal gameEndOfTerm


    Connections {
        target: gameHS
        onGameStartConnect: {
            game_info.StartGame()
        }
    }

    Connections {
        target: gameHS
        onGameExitToMenu:  {
            gameHS.visible = false;
            mainMenu.visible = true;
        }
    }

    Connections {
        target: gameHS
        onGameEndOfTerm: {

                if (twoCardsCheck)
                {
                    twoCardsCheck=false
                    if (game_info.ternN == prevTern)
                    {
                        console.log("Cards Was Blocked")
                        game_info.BlockCard(game_info.numN_1)
                        game_info.BlockCard(game_info.numN_2)
                        if (prevTern)
                        game_info.IncreseScore()
                        else
                        game_info.IncreseScore1()

                    }
                    else
                    {
                        field.fieldCloseTwoCards()
                        prevTern = game_info.ternN;
                    }
                    field.fieldSwitchText()


                }
                field.fieldRunCards()
            }

    }

    Game_Inf_HS
    {
        id: game_info

        Connections {
            target: game_info
            onReadTwoNumbs: {
                field.fieldStopCards()
                //twoCardsCheck=true
                gameEndOfTerm()
            }
        }
        Connections {
            target: game_info
            onBlockGameCards: {
                field.fieldStopCards()
            }
        }
        Connections {
            target: game_info
            onUnblockGameCards: {
                field.fieldRunCards()
            }
        }
        Connections {
            target: game_info
            onChangeCardTypeGame: {
                field.fieldChangeCardType()
            }
        }
        Connections {
            target: game_info
            onFieldTextChange: {
                field.fieldSwitchText()
            }
        }
        Connections {
            target: game_info
            onOpenChosenCard: {
                field.fieldOpenChosenCard()
            }
        }
        Connections {
            target: game_info
            onCloseTwoCards: {
                field.fieldCloseTwoCards()
            }
        }
        Connections {
            target: game_info
            onNewGameCreation: {
                prevTern = true
                field.fieldCreateNewCardType()
            }
        }
        Connections {
            target: game_info
            onSetTwoCardsCheckTrue: {
                twoCardsCheck = true
            }
        }

    }

    Qml_FieldHS
    {
        id: field
    }




}
