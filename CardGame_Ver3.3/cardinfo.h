#ifndef CARDINFO_H
#define CARDINFO_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <random>

class CardInfo : public QQuickItem
{
    Q_OBJECT
private:
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int numN READ numN WRITE setNumN NOTIFY numNChanged)
    QString TopName;
    int num;
    int tesType;
public:
    CardInfo(QQuickItem* parent = 0);

    int numN();
    void setNumN(int t);
    int type();
    void setType(int t);
signals:
    void typeChanged();
    void numNChanged();
public slots:
    QString GetCardBack();
    QString GetTopName();
};

#endif // CARDINFO_H
