import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0

Item
{
    id: game
    property int sound: 0
    property bool twoCardsCheck:false

    signal gameStartConnect
    signal gameExitToMenu
    signal gameEndOfTerm

    Connections {
        target: game
        onGameStartConnect: {
            game_info.StartGame()
        }
    }

    Connections {
        target: game
        onGameExitToMenu:  {
            game.visible = false;
            disMes.visible = true;
        }
    }

    Connections {
        target: game
        onGameEndOfTerm: {

                if (twoCardsCheck)
                {
                    twoCardsCheck=false
                    if (game_info.ternN)
                    {
                        console.log("Cards Was Blocked")
                        game_info.BlockCard(game_info.numN_1)
                        game_info.BlockCard(game_info.numN_2)
                        game_info.IncreseScore()
                        field.fieldRunCards()
                        game_info.SendDataToServer();
                    }
                    else
                    {
                        field.fieldCloseTwoCards()
                        field.fieldStopCards()
                        game_info.SendDataToServerEnd();


                    }
                    field.fieldSwitchText()


                }
                else
                {
                    game_info.SendDataToServerTime()
                }
            }

    }

    Game_Inf
    {
        id: game_info

        Connections {
            target: game_info
            onReadTwoNumbs: {
                field.fieldStopCards()
                //twoCardsCheck=true
                gameEndOfTerm()
            }
        }
        Connections {
            target: game_info
            onBlockGameCards: {
                field.fieldStopCards()
            }
        }
        Connections {
            target: game_info
            onUnblockGameCards: {
                field.fieldRunCards()
            }
        }
        Connections {
            target: game_info
            onChangeCardTypeGame: {
                field.fieldChangeCardType()
            }
        }
        Connections {
            target: game_info
            onFieldTextChange: {
                field.fieldSwitchText()
            }
        }
        Connections {
            target: game_info
            onOpenChosenCard: {
                field.fieldOpenChosenCard()
            }
        }
        Connections {
            target: game_info
            onCloseTwoCards: {
                field.fieldCloseTwoCards()
            }
        }
        Connections {
            target: game_info
            onEndOfGame: {
                twoCardsCheck = false
                game.gameExitToMenu()
            }
        }
        Connections {
            target: game_info
            onNewGameCreation: {
                field.fieldCreateNewCardType()
            }
        }
        Connections {
            target: game_info
            onSetTwoCardsCheckTrue: {
                twoCardsCheck = true
            }
        }

    }

    Qml_Field
    {
        id: field
    }




}

