import QtQuick 2.4
import Cards 1.0

Flipable
{
    id: flipCard
    width: 100
    height: 140

    property bool flipped: false
    property bool flippedAble: true
    property int cardNomer
    property int cardType
    signal cardStopFlippe
    signal cardRunFlippe
    signal cardCloseRunCard
    signal cardOpenCard
    signal cardCloseCard
    signal cardRefreshCard

    Connections {
        target: flipCard
        onCardStopFlippe: {
            flippedAble = false
        }
    }

    Connections {
        target: flipCard
        onCardRunFlippe: {
            flippedAble = true
        }
    }

    Connections {
        target: flipCard
        onCardCloseRunCard: {
            flipped = false
            flippedAble = true
        }
    }

    Connections {
        target: flipCard
        onCardOpenCard:{
            flipped = true
        }
    }

    Connections {
        target: flipCard
        onCardCloseCard:{
            flipped = false
        }
    }

    Connections {
        target: flipCard
        onCardRefreshCard: {
            cardInf.type = cardType
            backImage.source = cardInf.GetCardBack()
        }
    }

    Card_Inf
    {
        id: cardInf
        numN: cardNomer
        type: cardType
    }

    front:
        Image{
        id:frontImage
        source: cardInf.GetTopName()
    }

    back:
        Image{
        id:backImage
        source: cardInf.GetCardBack()
    }

    transform:
    Rotation
    {
        id: rotation
        origin.x: flipCard.width/2
        origin.y: flipCard.height/2
        axis.x: 0; axis.y:1; axis.z: 0
        angle: 0
    }

    states:
    State
    {
        name: "Back"
        PropertyChanges { target: rotation; angle: 180 }
        when: flipCard.flipped
    }

    transitions:
    Transition
    {
        NumberAnimation
        {
            target: rotation; property: "angle"; duration: 1000
        }
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            if (flippedAble)
            {

                console.log("CardNum "+ cardInf.numN)
                console.log("TypeNum "+ cardInf.type)
                flipCard.flipped = !flipCard.flipped
                cardStopFlippe()
                game_info.GetCard(cardInf.type,cardInf.numN)
            }
            else
            {
                console.log("Card is Blocked")
            }
                console.log("\n\n")
        }

    }

}
