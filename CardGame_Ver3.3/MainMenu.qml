import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

Item
{
    signal start
    signal options
    signal howtoplay
    signal startnetwork

    Connections {
        onStart:
        {
            visible = false;
            game.visible = true;

        }

        onOptions:
        {
            visible = false;
            optionsMenu.visible = true;
        }

        onHowtoplay:
        {
            visible = false;
            howtoplayMenu.visible = true;
        }

        onStartnetwork:
        {
            visible = false;
            network.visible = true;
            network.gameStartConnect()
        }
    }
    ColumnLayout
    {
        anchors.fill: parent
        spacing: 10

        Text
        {
            text: qsTr("Find a couple!");

            font
            {
                bold: true
                family: "Verdana"
                pointSize: 14
            }

            Layout.alignment: Qt.AlignHCenter
        }

        Item
        {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Block
            {
                color: "yellow"
                figure: [
                    [0,0,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,1,1,0,0,0,0],
                    [0,0,1,0,0,0,0,0,1,0,0,1,1,0,0,1,0,1,0,1,0,0,0],
                    [0,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,0],
                    [0,0,1,0,0,0,0,0,1,0,0,1,0,0,1,1,0,1,0,1,0,0,0],
                    [0,0,1,0,0,0,0,1,1,1,0,1,0,0,0,1,0,1,1,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0],
                    [1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
                    [1,0,0,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,0,0],
                    [1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,1,0],
                    [1,0,0,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,0,0],
                    [1,1,1,0,1,1,1,0,0,1,1,0,1,0,0,0,1,1,1,0,1,1,1]
                ]

                anchors.centerIn: parent
            }
        }

        Qml_MyButton
        {
            text: qsTr("New game")

            Layout.fillWidth: true

            onClicked:
            {
                start();
            }
        }

        Qml_MyButton
        {
            text: qsTr("New Network Game")

            Layout.fillWidth: true

            onClicked:
            {
                startnetwork();
            }
        }

        Qml_MyButton
        {
            text: qsTr("Options")

            Layout.fillWidth: true

            onClicked:
            {
                options();
            }
        }

        Qml_MyButton
        {
            text: qsTr("How to play")

            Layout.fillWidth: true

            onClicked:
            {
                howtoplay();
            }
        }

        Qml_MyButton
        {
            text: qsTr("Quit game")

            Layout.fillWidth: true

            onClicked:
            {
                Qt.quit();
            }
        }
    }
}



