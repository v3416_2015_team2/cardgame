import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

Item
{
    id: howtoplaymenu

    signal read


    Image
    {
            source: "howtoplay"
            anchors.fill: parent
    }

    Qml_MyButton
    {
        text: qsTr("Back to main menu")

        //Layout.columnSpan: 2

        //Layout.fillWidth: true
        anchors.horizontalCenter: parent.horizontalCenter
        y: 1000

        onClicked: read()
    }

    Text
    {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 13
        color: "black"
        text:
        "Добро пожаловать в игру Find a couple!

На игровом поле расположены карточки.
За один ход игрок вскрывает
любые две из них. В случае, если
изображения на них совпали, карточки
убираются, а игрок получает на свой
счет очки. Иначе карточки
возвращаются на исходные позиции, а
ход переходит к оппоненту. Игра
продолжается до тех пор, пока на
игровом поле есть карточки.
Победителем
становится игрок, у которого к
концу игры набирается большее
количество
очков."
    }
}

