#ifndef GameInfoHS_H
#define GameInfoHS_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <random>
#include <QTime>
#include <vector>
#include "playerinfo.h"
using namespace std;
class GameInfoHS : public QQuickItem
{
    Q_OBJECT
protected:
    Q_PROPERTY(int numN_1 READ numN_1 WRITE setNumN_1 NOTIFY numN_1Changed)
    Q_PROPERTY(int numN_2 READ numN_2 WRITE setNumN_2 NOTIFY numN_2Changed)
    Q_PROPERTY(bool ternN READ ternN WRITE setTernN NOTIFY ternNChanged)
    Q_PROPERTY(int plOneScore READ plOneScore WRITE setPlOneScore NOTIFY plOneScoreChanged)
    Q_PROPERTY(int plTwoScore READ plTwoScore WRITE setPlTwoScore NOTIFY plTwoScoreChanged)
    vector<int> typeVec;
    bool ternNum;
    PlayerInfo *firPlayer;
    PlayerInfo *secPlayer;
    int firstType;
    int seconType;
    int firstNumb;
    int seconNumb;
    bool boolForBloc[30] ;
public:
    GameInfoHS();
    bool IsHit();


    int numN_1();
    void setNumN_1(int t);
    int numN_2();
    void setNumN_2(int t);
    bool ternN();
    void setTernN(bool t);
    int plOneScore();
    void setPlOneScore(int n);
    int plTwoScore();
    void setPlTwoScore(int n);

signals:
    void numN_1Changed();
    void numN_2Changed();
    void ternNChanged();
    void readTwoNumbs();
    void switchPlayer();
    void plOneScoreChanged();
    void plTwoScoreChanged();

public slots:
    int WhoWins();
    bool IsEndOfGame();
    void BlockCard(int n);
    bool PermisionForFlip(int num);
    int GetCardType();
    bool WaitSecCard();
    void GetCard(int type,int num);
    void Sleep();
    void IncreseScore();
    void IncreseScore1();
};

#endif // GameInfoHS_H
