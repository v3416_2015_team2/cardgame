#include "gameinfoHS.h"

GameInfoHS::GameInfoHS()
{
    for (int i=0; i < 30; i++)
        boolForBloc[i]= false;

    firstType = NULL;
    seconType = NULL;
    firPlayer = new PlayerInfo(1);
    secPlayer = new PlayerInfo(2);
    ternNum = 1;//2
    for (int i =1; i < 16; i++)
    {
        typeVec.push_back(i);
        typeVec.push_back(i);
    }
}
int GameInfoHS::GetCardType()
{
    int sizen = typeVec.size();

    int t = (rand()%30);
    int start = t;
    while (typeVec[t] == -1)
    {
        t++;
        if (t == sizen)
        {
            t=0;
        }
        if (start == t)
        {
            break;
        }
    }
    sizen = typeVec[t];
    typeVec[t] = -1;
    qDebug() << "Set Type" << sizen;
    return sizen;
}
bool GameInfoHS::IsHit()
{
   if (firstType == seconType)
       return true;
   return false;
}

bool GameInfoHS::IsEndOfGame()
{
    qDebug()<<"--" << firPlayer->scorN();
    for (int i =0; i < 30; i ++)
    {
        if (!boolForBloc[i])
            return false;
    }
    return true;
}

void GameInfoHS::BlockCard(int n)
{
    boolForBloc[n] = true;
}

void GameInfoHS::Sleep()
{
    QTime time;
    time.start();
    for(;time.elapsed() < 1000;)
    {

    }
}

int GameInfoHS::numN_1()
{
    return firstNumb;
}

void GameInfoHS::setNumN_1(int t)
{
    firstNumb = t;
}
int GameInfoHS::numN_2()
{
    return seconNumb;
}
void GameInfoHS::setNumN_2(int t)
{
    seconNumb = t;
}

bool GameInfoHS::ternN()
{
    return ternNum;
}

void GameInfoHS::setTernN(bool t)
{
    ternNum = t;
}

int GameInfoHS::plOneScore()
{
    return firPlayer->scorN();
}

void GameInfoHS::setPlOneScore(int n)
{
    firPlayer->setScorN(n);
}

int GameInfoHS::plTwoScore()
{
    return secPlayer->scorN();
}

void GameInfoHS::setPlTwoScore(int n)
{
    secPlayer->setScorN(n);
}

int GameInfoHS::WhoWins()
{
    qDebug() << firPlayer->scorN()<<"_____"<<secPlayer->scorN();
    if (firPlayer->scorN() == secPlayer->scorN())
        return 0;
    else
    {
        if (firPlayer->scorN() > secPlayer->scorN())
            return 1;
        else
            return 2;
    }
}

bool GameInfoHS::PermisionForFlip(int num)
{
    return boolForBloc[num];
}
bool GameInfoHS::WaitSecCard()
{
    if (seconType==NULL)
    {
        return false;
    }
    return true;
}

void GameInfoHS::GetCard(int type,int num)
{

    if (firstType == NULL)
    {
        firstNumb = num;
        firstType = type;
    }
    else
    {
        seconNumb = num;
        seconType = type;
        if (IsHit())
        {
            qDebug()<<"BINGO";
        }
        else
        {
          qDebug()<<"Sorry,NOT";
          ternNum=!ternNum;
        }
        readTwoNumbs();
        firstType = NULL;
        seconType = NULL;
    }
}

void GameInfoHS::IncreseScore()
{

    firPlayer->addScore();
}

void GameInfoHS::IncreseScore1()
{
    secPlayer->addScore();
}
