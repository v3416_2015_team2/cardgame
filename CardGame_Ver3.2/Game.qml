import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0

Item
{
    id: game

   // width: 720
   // height: 1280
    property int numFir
    property int numSec
    property int sound: 0
    signal exitToMenu
    signal getFirNum

    Game_Inf_HS
    {
        id: game_info
        Connections {
            target: game_info
            onReadTwoNumbs: {

                game.numFir = game_info.numN_1
                game.numSec = game_info.numN_2
                //PauseAnimation { duration: 100 }
                field.closeChoseCards()
            }
        }

    }
    Player_Inf
    {
       id: plOne

    }
    Player_Inf
    {
        id: plTwo

    }
    Field
    {
        id: field
    }




}

