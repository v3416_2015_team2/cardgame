import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0
Item {

    id: field
    width: 720
    height: 1280
    property int iTest:0
    property bool prevTern:true
    property bool boolForBloc:game_info.ternN
    property bool twoCardsCheck:false
    property string cardName: "card_0"
    property variant items:[card_0,card_1,card_2,card_3,card_4,card_5,card_6,card_7,card_8,card_9,
                            card_10,card_11,card_12,card_13,card_14,card_15,card_16,card_17,card_18,card_19,
                            card_20,card_21,card_22,card_23,card_24,card_25,card_26,card_27,card_28,card_29]
    //signal exitToMenu
    signal testSignal
    signal closeChoseCards
    signal blockCards
    Connections {
        target: field
        onTestSignal: {
            for ( iTest=0; iTest<30;iTest++)
            {
                //console.log(itemsBlockBool[iTest])
                if (!game_info.PermisionForFlip(iTest))
                items[iTest].runFlippe()

            }
        }
    }
    Connections {
        target: field
        onCloseChoseCards: {
            field.blockCards()
            twoCardsCheck=true
        }
    }
    Connections {
        target: field
        onBlockCards: {
            for ( iTest=0; iTest<30;iTest++)
            {
                items[iTest].stopFlippe()
            }
        }
    }
    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            if (twoCardsCheck)
            {
                //game_info.Sleep()
                twoCardsCheck=false
                if (prevTern == game_info.ternN)
                {
                    console.log("Cards Was Blocked")
                    game_info.BlockCard(game.numFir)

                    game_info.BlockCard(game.numSec)
                    //items[game.numFir].visible=false
                    //items[game.numSec].visible=false
                    if(!game_info.ternN)
                        game_info.IncreseScore1()
                    else
                        game_info.IncreseScore()
                }
                else
                {
                    items[game.numFir].closeCard()
                    items[game.numSec].closeCard()
                }
                prevTern = game_info.ternN
                textMes.text = qsTr("SecondPlayerTurn")
                textScore.text = qsTr(""+game_info.plTwoScore)
                if (game_info.ternN)
                {
                   textMes.text= qsTr("FirstPlayerTurn")
                   textScore.text = qsTr(""+game_info.plOneScore)
                }
                if (game_info.IsEndOfGame())
                {
                    textMes.text= qsTr("GAME OVER")
                    if (game_info.plOneScore == game_info.plOneScore)
                        textScore.text = qsTr("Draw "+game_info.plOneScore)
                    else
                        if  (plOne.scorN > plTwo.scorN)
                            textScore.text = qsTr("FirstPlayer WINS "+game_info.plOneScore)
                        else
                            textScore.text = qsTr("SecondPlayer WINS "+game_info.plTwoScore)
                }

                field.testSignal()
            }
        }
    }
    Text {
        id: textMes
        text: qsTr("FirstPlayerTurn")
        y:0
        x:parent.width/2
    }
    Text {
        id: textScore
        text: qsTr(""+game_info.plOneScore)
        y:10
        x:parent.width/2+300
    }

    Row
    {
        //anchors.centerIn: parent
        spacing: 50
        Card
        {
            id:card_0
            x:parent.width/2 + 10
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:0
        }

        Card
        {
            id:card_1
            x:parent.width/2 + 10
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:1
        }

        Card
        {
            id:card_2
            x:parent.width/2 + 10
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:2
        }

        Card
        {
            id:card_3
            x:parent.width/2 + 10
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:3
        }

        Card
        {
            id:card_4
            x:parent.width/2 + 10
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:4
        }
    }

    Row
    {
        spacing: 50

        Card
        {
            id:card_5
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:5
        }

        Card
        {
            id:card_6
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:6
        }

        Card
        {
            id:card_7
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:7
        }

        Card
        {
            id:card_8
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:8
        }

        Card
        {
            id:card_9
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:9
        }
    }

    Row
    {
        spacing: 50

        Card
        {
            id:card_10
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:10
        }

        Card
        {
            id:card_11
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:11
        }

        Card
        {
            id:card_12
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:12
        }

        Card
        {
            id:card_13
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:13
        }

        Card
        {
            id:card_14
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:14
        }
    }

    Row
    {
        spacing: 50

        Card
        {
            id:card_15
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:15
        }

        Card
        {
            id:card_16
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:16
        }

        Card
        {
            id:card_17
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:17
        }

        Card
        {
            id:card_18
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:18
        }

        Card
        {
            id:card_19
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:19
        }
    }

    Row
    {
        spacing: 50

        Card
        {
            id:card_20
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:20
        }

        Card
        {
            id:card_21
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:21
        }

        Card
        {
            id:card_22
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:22
        }

        Card
        {
            id:card_23
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:23
        }

        Card
        {
            id:card_24
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:24
        }
    }

    Row
    {
        spacing: 50

        Card
        {
            id:card_25
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:25
        }

        Card
        {
            id:card_26
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:26
        }

        Card
        {
            id:card_27
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:27
        }

        Card
        {
            id:card_28
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:28
        }

        Card
        {
            id:card_29
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:29
        }
    }

    Qml_MyButton
    {
        text: qsTr("Back to main menu")
        x: 320
        y: 1100
        onClicked: exitToMenu()
    }

    Qml_MyButton
    {
        text: qsTr("Next")
        x: 100
        y: 1100
        onClicked: testSignal()
    }
}

