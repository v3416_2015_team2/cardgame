import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0
Item {
    id: field
    width: 720
        height: 1280
    property int iTest:0
    property variant items:[card_0,card_1,card_2,card_3,card_4,card_5,card_6,card_7,card_8,card_9,
                            card_10,card_11,card_12,card_13,card_14,card_15,card_16,card_17,card_18,card_19,
                            card_20,card_21,card_22,card_23,card_24,card_25,card_26,card_27,card_28,card_29]

    signal fieldRunCards
    signal fielStopCards
    signal fieldSwitchText
    signal fieldOpenChosenCard
    signal fieldChangeCardType
    signal fieldCloseTwoCards

    Connections {
        target: field
        onFieldRunCards: {
            for ( iTest=0; iTest<30;iTest++)
            {
                if (!game_info.PermisionForFlip(iTest))
                items[iTest].cardRunFlippe()
            }
        }
    }

    Connections {
        target: field
        onFielStopCards: {
            for ( iTest=0; iTest<30;iTest++)
            {
                items[iTest].cardStopFlippe()
            }
        }
    }

    Connections {
        target: field
        onFieldSwitchText: {
            if (game_info.ternN)
            {
               textMes.text= qsTr("Your Turn")

            }
            else
            {
                textMes.text = qsTr("Your Opponent's Turn")
            }
            textScore.text = qsTr("Yor Score: "+game_info.plOneScore + "  Opponent Score: "+game_info.plTwoScore)
            if (game_info.IsEndOfGame())
            {
                textMes.text= qsTr("GAME OVER")
                switch (game_info.WhoWins())
                {
                case 0:
                    textScore.text = qsTr("Draw "+game_info.plOneScore)
                    break
                case 1:
                    textScore.text = qsTr("FirstPlayer WINS " + game_info.plOneScore)
                    break
                case 2:
                    textScore.text = qsTr("SecondPlayer WINS " + game_info.plTwoScore)
                }
            }
        }
    }

    Connections {
        target: field
        onFieldOpenChosenCard: {
                items[game_info.numN_1].cardOpenCard();
        }
    }

    Connections {
        target: field
        onFieldChangeCardType: {
            for ( iTest=0; iTest<30;iTest++)
            {
                items[iTest].cardType = game_info.GetChangeCardType()
                items[iTest].cardRefreshCard();
            }
        }
    }

    Connections {
        target: field
        onFieldCloseTwoCards: {
            items[game_info.numN_1].cardCloseRunCard()
            items[game_info.numN_2].cardCloseRunCard()
        }
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            console.log("SpaceCkick")
        }
    }

    Text {
        id: textMes
        text: qsTr("Yor Turn")
        y:0
        x:parent.width/2-15
    }
    Text {
        id: textScore
        text: qsTr("Yor Score: "+game_info.plOneScore + "  Opponent Score: "+game_info.plTwoScore)
        y:10
        x:parent.width/2-15
    }

    Row
    {
        spacing: 30
        Qml_Card
        {
            id:card_0
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:0
        }

        Qml_Card
        {
            id:card_1
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:1
        }

        Qml_Card
        {
            id:card_2
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:2
        }

        Qml_Card
        {
            id:card_3
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:3
        }

        Qml_Card
        {
            id:card_4
            y:parent.height/8 + 30
            cardType: game_info.GetCardType()
            cardNomer:4
        }
    }

    Row
    {
        spacing: 30

        Qml_Card
        {
            id:card_5
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:5
        }

        Qml_Card
        {
            id:card_6
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:6
        }

        Qml_Card
        {
            id:card_7
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:7
        }

        Qml_Card
        {
            id:card_8
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:8
        }

        Qml_Card
        {
            id:card_9
            y:parent.height/8 + 195
            cardType: game_info.GetCardType()
            cardNomer:9
        }
    }

    Row
    {
        spacing: 30

        Qml_Card
        {
            id:card_10
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:10
        }

       Qml_Card
        {
            id:card_11
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:11
        }

       Qml_Card
        {
            id:card_12
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:12
        }

       Qml_Card
        {
            id:card_13
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:13
        }

       Qml_Card
        {
            id:card_14
            y:parent.height/8 + 370
            cardType: game_info.GetCardType()
            cardNomer:14
        }
    }

    Row
    {
        spacing: 30

       Qml_Card
        {
            id:card_15
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:15
        }

       Qml_Card
        {
            id:card_16
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:16
        }

       Qml_Card
        {
            id:card_17
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:17
        }

       Qml_Card
        {
            id:card_18
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:18
        }

       Qml_Card
        {
            id:card_19
            y:parent.height/8 + 545
            cardType: game_info.GetCardType()
            cardNomer:19
        }
    }

    Row
    {
        spacing: 30

       Qml_Card
        {
            id:card_20
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:20
        }

       Qml_Card
        {
            id:card_21
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:21
        }

       Qml_Card
        {
            id:card_22
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:22
        }

       Qml_Card
        {
            id:card_23
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:23
        }

       Qml_Card
        {
            id:card_24
            y:parent.height/8 + 720
            cardType: game_info.GetCardType()
            cardNomer:24
        }
    }

    Row
    {
        spacing: 30

       Qml_Card
        {
            id:card_25
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:25
        }

       Qml_Card
        {
            id:card_26
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:26
        }

       Qml_Card
        {
            id:card_27
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:27
        }

       Qml_Card
        {
            id:card_28
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:28
        }

       Qml_Card
        {
            id:card_29
            y:parent.height/8 + 895
            cardType: game_info.GetCardType()
            cardNomer:29
        }
    }
    Button
    {
        text: qsTr("Back to main menu")
        x: parent.width-90
        y: 700
        onClicked:  game.gameExitToMenu()
    }
    //Дополнительна, Кнопка перехода хода, Костыль
    Button
    {
        text: qsTr("TestButton")
        x: parent.width-90
        y: 750
        onClicked: {

            game_info.ternN = true
            fieldRunCards()
            field.fieldSwitchText()
        }
    }



}

