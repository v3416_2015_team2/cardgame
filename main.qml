//import Shapes 1.0
import QtQuick 2.2
import QtQuick.Window 2.1
import Cards 1.0


Window {
    id: test
    visible: true
    width: 720
    height: 800

    minimumWidth: 720
    minimumHeight: 800

    title: qsTr("FIND A COUPLE")

    Image
    {
            source: "front"
            anchors.fill: parent
    }



    MainMenu
        {
            id: mainMenu

            anchors.fill: parent
            anchors.margins: 20

            onStart:
            {
                visible = false;
                game.visible = true;

                //game.start(optionsMenu.sound);
                //,optionsMenu.facade);
            }

            onOptions:
            {
                visible = false;
                optionsMenu.visible = true;
            }

            onHowtoplay:
            {
                visible = false;
                howtoplayMenu.visible = true;
            }

            onStartnetwork:
            {
                visible = false;
                network.visible = true;
            }
        }

        OptionsMenu
        {
            id: optionsMenu

            visible: false

            anchors.fill: parent
            anchors.margins: 20

            onDone:
            {
                visible = false;
                mainMenu.visible = true;
            }
        }

        HowToPlayMenu
        {
            id: howtoplayMenu

            visible: false

            anchors.fill: parent
            anchors.margins: 20

            onRead:
            {
                visible = false;
                mainMenu.visible = true;
            }
        }

        Game
           {
               id: game

               visible: false

               anchors.fill: parent

               onExitToMenu:
               {
                   visible = false;
                   mainMenu.visible = true;
               }
           }


    /*MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("CLICK" )
            visible = false;
            game.visible = true;
        }
    }*/
    /*Text {
        text: qsTr("Press me to START")
        anchors.centerIn: parent
    }*/
    /*Game
    {
        id: game
        visible: false
        anchors.fill: parent
        onExitToMenu:
        {
            //console.log("Received in QML from C++iii: " )
            game.visible = false;
            test.visible = true;
        }
    }*/

}
