#include "netconnect.h"

NetConnect::NetConnect(QObject *parent,GameInfo *inGInf) : QObject(parent)
{
    qDebug() << "1. Creation";
    //KOCTblb=true;//$$
    connGameInf = inGInf;
    _pSocket = new QTcpSocket( this );
    _pSocket->connectToHost("127.0.0.1", 1234);//QHostAddress::Any
    connect( _pSocket, SIGNAL(connected()),this,SLOT(connectTcp()));
    connect( _pSocket, SIGNAL(readyRead()),this,SLOT(readTcp()) );
    connect( _pSocket, SIGNAL(disconnected()),this,SLOT(disconnectTcp()));
    if(!_pSocket->waitForConnected(5000))
    {
            qDebug() << "Error: " << _pSocket->errorString();
            _pSocket->write("ERROR");
            connGameInf->openDiscMessage();

    }

}

void NetConnect::connectTcp()
{
    //if (KOCTblb)//$$
    //{//$$
        //KOCTblb = false;//$$
        qDebug() << "2. Connections";
        //writeData();
   // }


}

void NetConnect::disconnectTcp()
{
    qDebug() << "3. Disconnections";
    _pSocket->close();
}

void NetConnect::writeData()
{
    qDebug() << "5. WriteDefalutData";
    socData = "DATATEXT";
    //qDebug() <<( socData );
    _pSocket->write( socData );
    //socData = "SecondData???";
    //qDebug() <<( socData );
}

void NetConnect::writeComeData(QByteArray inputData)
{
    qDebug() << "6. WriteComeData";

    _pSocket->write( inputData );
}

void NetConnect::readTcp()
{
    qDebug() << "4. Read";
    QByteArray readData;

    readData=_pSocket->readAll();
    connGameInf->GetMassage(readData);


}

