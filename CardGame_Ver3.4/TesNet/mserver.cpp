#include "mserver.h"

MyServer::MyServer(QMainWindow *parent) :
    QMainWindow(parent)

{
    qDebug() << "TEST SERVER RUNS";
    ammOfSocets=0;
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), this, SLOT(getNewConnection()));
    if (!server->listen(QHostAddress::Any,1234))
    {
      qDebug() << "Couldnt Start";
    }
    else
    {
        qDebug() << "Started!";

    }

}
void MyServer::getNewConnection() // обработчик подключений
{
    QTcpSocket * socket = server->nextPendingConnection();
    ammOfSocets++;
    qDebug() << "Client Connected";

    if (ammOfSocets == 1)
    {
        connect(socket, SIGNAL(readyRead ()), this, SLOT(doRead()));
        connect(socket, SIGNAL(disconnected()),this, SLOT(doDiscon()));
        firstSocket = socket;
        firstSocket->write("000");
    }
    else
    {
        if (ammOfSocets == 2)
        {
            connect(socket, SIGNAL(readyRead ()), this, SLOT(doRead_2()));
            connect(socket, SIGNAL(disconnected()),this, SLOT(doDiscon_2()));
            secondSocket = socket;
            secondSocket->write("001" + firPlData);
            firstSocket->write("005");
        }
        else
        {
            qDebug() << "CantDoMoreConnects";
            ammOfSocets--;
            socket->write("006");

        }
    }


}

void MyServer::doRead()
{
    qDebug() << "Server Read 1";
    firPlData =  firstSocket->readAll();
    qDebug() <<firPlData;


    QByteArray tes = "";
    tes = tes +firPlData[0]+firPlData[1]+firPlData[2];
    QString stes (tes) ;
    int tN = stes.toInt();
    if ((tN == 2)||(tN == 3)||(tN == 4))
    secondSocket->write(firPlData);
    if (tN == 10)
    {
        doDiscon();
    }
}

void MyServer::doDiscon()
{
    qDebug() << "Server Delete";
    ammOfSocets = 0;
    firstSocket->close();

    if (ammOfSocets == 2)
    {
        secondSocket->write("007");
        secondSocket->close();
    }

}

void MyServer::doRead_2()
{
    qDebug() << "Server Read 2";
    secPlData =  secondSocket->readAll();
    qDebug() <<secPlData;

    QByteArray tes = "";
    tes = tes +secPlData[0]+secPlData[1]+secPlData[2];
    QString stes (tes) ;
    int tN = stes.toInt();
    if ((tN == 2)||(tN == 3)||(tN == 4))
    firstSocket->write(secPlData);
    if (tN == 10)
    {
        doDiscon_2();
    }
}

void MyServer::doDiscon_2()
{
    qDebug() << "Server Delete 2";
    ammOfSocets = 0;
    secondSocket->close();
    firstSocket->write("007");
    firstSocket->close();
}

void MyServer::doSendDataToFir()
{

}

void MyServer::doSendDataToSec()
{

}
