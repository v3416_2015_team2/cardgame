#ifndef NETCONNECT_H
#define NETCONNECT_H

#include <QObject>
//#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QByteArray>
//#include <QDataStream>
#include "gameinfo.h"
class GameInfo;

class NetConnect : public QObject
{
    Q_OBJECT
private :
    QTcpSocket *_pSocket;
    QByteArray socData;
    bool KOCTblb;//$$
    GameInfo * connGameInf;
    int connectionNum;
public:
    explicit NetConnect(QObject *parent = 0,GameInfo  *inf = 0);

signals:
    void readyRead();

public slots:
    void readTcp();
    void connectTcp();
    void disconnectTcp();
    void writeData();
    void writeComeData(QByteArray);
};

#endif // NETCONNECT_H
