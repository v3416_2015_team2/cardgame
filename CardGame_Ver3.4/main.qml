import QtQuick 2.2
import QtQuick.Window 2.1
import Cards 1.0


Window {
    id: test
    visible: true
    width: 720
    height: 1000//1280

    minimumWidth: 720
    minimumHeight: 1000//1280

    title: qsTr("FIND A COUPLE")

    Image
    {
        id:front_Image
        source: "front"
        anchors.fill: parent
    }

    MainMenu
    {
            id: mainMenu

            anchors.fill: parent
            anchors.margins: 20


    }
    Qml_MassageDisconnect
    {

        id: disMes
        visible: false
        anchors.fill: parent
    }
    Qml_MassageWait
    {

        id: waitMes
        visible: false
        anchors.fill: parent
    }
    Qml_GameHS
       {
           id: game
           visible: false
           anchors.fill: parent
       }
    Qml_NetGame
    {
        id: network
        visible: false
        anchors.fill: parent


    }
    OptionsMenu
    {
        id: optionsMenu

        visible: false

        anchors.fill: parent
        anchors.margins: 20

        onDone:
        {
            visible = false;
            mainMenu.visible = true;
        }
    }
    Qml_HowToPlayMenu
    {
        id: howtoplayMenu

        visible: false

        anchors.fill: parent
        anchors.margins: 20

        onRead:
        {
            visible = false;
            mainMenu.visible = true;
        }
    }




}
