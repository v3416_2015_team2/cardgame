#ifndef MSERVER_H
#define MSERVER_H

#include <QMainWindow>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QByteArray>
#include <QDataStream>
#include <QString>
#include <vector>
using namespace std;

class MyServer : public QMainWindow
{
    Q_OBJECT
private:
    QTcpServer *server; // указатель на сервер
    QList<QTcpSocket *> sockets; // получатели данных
    QTcpSocket *firstSocket;
    QTcpSocket *secondSocket;// вещатель
    int ammOfSocets;
    QByteArray firPlData;
    QByteArray secPlData;

    QList <QTcpSocket *> firstSocketVec;
    QList <QTcpSocket *> secondSocketVec;
    QList <QByteArray> firPlDataVec;
    QList <QByteArray> secPlDataVec;
    int endPosition;
public:
    explicit MyServer(QMainWindow *parent = 0); // конструктор
signals:
public slots:
    void getNewConnection(); // обработчик входящего подключения
    void doRead();
    void doDiscon();
    void doRead_2();
    void doDiscon_2();
    void doSendDataToFir();
    void doSendDataToSec();

};

#endif // MSERVER_H
