#ifndef GAMEINFO_H
#define GAMEINFO_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <QByteArray>
#include <QTime>
#include <QTimer>
#include <QTimerEvent>
#include <random>
#include <vector>
#include "playerinfo.h"
#include "netconnect.h"

using namespace std;
class NetConnect;
class GameInfo : public QQuickItem
{
    Q_OBJECT
protected:
    Q_PROPERTY(int numN_1 READ numN_1 WRITE setNumN_1 NOTIFY numN_1Changed)
    Q_PROPERTY(int numN_2 READ numN_2 WRITE setNumN_2 NOTIFY numN_2Changed)
    Q_PROPERTY(bool ternN READ ternN WRITE setTernN NOTIFY ternNChanged)
    Q_PROPERTY(int plOneScore READ plOneScore WRITE setPlOneScore NOTIFY plOneScoreChanged)
    Q_PROPERTY(int plTwoScore READ plTwoScore WRITE setPlTwoScore NOTIFY plTwoScoreChanged)

    vector<int> typeVec;
    bool ternNum;
    PlayerInfo *firPlayer;
    PlayerInfo *secPlayer;
    NetConnect *serverCon;
    int firstType;
    int seconType;
    int firstNumb;
    int seconNumb;
    int position;
    bool boolForBloc[30] ;
    int typeOfCards[30];
    bool firstGame;
public:
    GameInfo();
    bool IsHit();
    void GetMassage(QByteArray data);
    void timerEvent(QTimerEvent *);

    int numN_1();
    void setNumN_1(int t);
    int numN_2();
    void setNumN_2(int t);
    bool ternN();
    void setTernN(bool t);
    int plOneScore();
    void setPlOneScore(int n);
    int plTwoScore();
    void setPlTwoScore(int n);


signals:
    void numN_1Changed();
    void numN_2Changed();
    void ternNChanged();
    void plOneScoreChanged();
    void plTwoScoreChanged();

    void readTwoNumbs();
    void switchPlayer();
    void blockGameCards();
    void unblockGameCards();
    void changeCardTypeGame();
    void fieldTextChange();
    void openChosenCard();
    void closeTwoCards();
    void endOfGame();
    void newGameCreation();
    void setTwoCardsCheckTrue();
    void openWaitMessage();
    void closeWaitMessage();
    void openDiscMessage();

public slots:

    bool IsEndOfGame();
    bool WaitSecCard();
    bool PermisionForFlip(int num);
    int WhoWins();
    int GetCardType();
    int GetChangeCardType();
    void BlockCard(int n);
    void GetCard(int type,int num);
    void IncreseScore();
    void IncreseScore1();
    void Sleep();
    void SendDataToServerEnd();
    void SendDataToServer();
    void SendDataToServerTime();
    void SendDataToServerDisconect();
    void StartGame();
    void CreateNewGame();
};

#endif // GAMEINFO_H
