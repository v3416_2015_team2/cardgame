TEMPLATE = app

QT += qml quick gui core declarative network
CONFIG += c++11

SOURCES += main.cpp \
    cardinfo.cpp \
    gameinfo.cpp \
    gameinfohs.cpp \
    netconnect.cpp \
    playerinfo.cpp \
    log.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    cardinfo.h \
    gameinfo.h \
    gameinfohs.h \
    netconnect.h \
    playerinfo.h \
    log.h
