#include "checkload.h"

CheckLoad::CheckLoad(QObject *parent)
    : QObject(parent),
      look_error(false) {}

CheckLoad::~CheckLoad() {}

bool CheckLoad::IfError() const
{
    return look_error;
}

void CheckLoad::ComponentStatusChanged(QObject* result, const QUrl&)
{
    if(!result)
        look_error = true;
}

