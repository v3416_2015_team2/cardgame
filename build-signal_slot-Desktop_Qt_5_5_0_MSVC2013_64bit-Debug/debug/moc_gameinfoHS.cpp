/****************************************************************************
** Meta object code from reading C++ file 'gameinfoHS.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../CardGame_Ver3.3/gameinfoHS.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gameinfoHS.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GameInfoHS_t {
    QByteArrayData data[36];
    char stringdata0[427];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GameInfoHS_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GameInfoHS_t qt_meta_stringdata_GameInfoHS = {
    {
QT_MOC_LITERAL(0, 0, 10), // "GameInfoHS"
QT_MOC_LITERAL(1, 11, 13), // "numN_1Changed"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 13), // "numN_2Changed"
QT_MOC_LITERAL(4, 40, 12), // "ternNChanged"
QT_MOC_LITERAL(5, 53, 17), // "plOneScoreChanged"
QT_MOC_LITERAL(6, 71, 17), // "plTwoScoreChanged"
QT_MOC_LITERAL(7, 89, 12), // "readTwoNumbs"
QT_MOC_LITERAL(8, 102, 12), // "switchPlayer"
QT_MOC_LITERAL(9, 115, 14), // "blockGameCards"
QT_MOC_LITERAL(10, 130, 16), // "unblockGameCards"
QT_MOC_LITERAL(11, 147, 18), // "changeCardTypeGame"
QT_MOC_LITERAL(12, 166, 15), // "fieldTextChange"
QT_MOC_LITERAL(13, 182, 14), // "openChosenCard"
QT_MOC_LITERAL(14, 197, 13), // "closeTwoCards"
QT_MOC_LITERAL(15, 211, 15), // "newGameCreation"
QT_MOC_LITERAL(16, 227, 20), // "setTwoCardsCheckTrue"
QT_MOC_LITERAL(17, 248, 11), // "IsEndOfGame"
QT_MOC_LITERAL(18, 260, 11), // "WaitSecCard"
QT_MOC_LITERAL(19, 272, 16), // "PermisionForFlip"
QT_MOC_LITERAL(20, 289, 3), // "num"
QT_MOC_LITERAL(21, 293, 7), // "WhoWins"
QT_MOC_LITERAL(22, 301, 11), // "GetCardType"
QT_MOC_LITERAL(23, 313, 9), // "BlockCard"
QT_MOC_LITERAL(24, 323, 1), // "n"
QT_MOC_LITERAL(25, 325, 7), // "GetCard"
QT_MOC_LITERAL(26, 333, 4), // "type"
QT_MOC_LITERAL(27, 338, 12), // "IncreseScore"
QT_MOC_LITERAL(28, 351, 13), // "IncreseScore1"
QT_MOC_LITERAL(29, 365, 5), // "Sleep"
QT_MOC_LITERAL(30, 371, 13), // "CreateNewGame"
QT_MOC_LITERAL(31, 385, 6), // "numN_1"
QT_MOC_LITERAL(32, 392, 6), // "numN_2"
QT_MOC_LITERAL(33, 399, 5), // "ternN"
QT_MOC_LITERAL(34, 405, 10), // "plOneScore"
QT_MOC_LITERAL(35, 416, 10) // "plTwoScore"

    },
    "GameInfoHS\0numN_1Changed\0\0numN_2Changed\0"
    "ternNChanged\0plOneScoreChanged\0"
    "plTwoScoreChanged\0readTwoNumbs\0"
    "switchPlayer\0blockGameCards\0"
    "unblockGameCards\0changeCardTypeGame\0"
    "fieldTextChange\0openChosenCard\0"
    "closeTwoCards\0newGameCreation\0"
    "setTwoCardsCheckTrue\0IsEndOfGame\0"
    "WaitSecCard\0PermisionForFlip\0num\0"
    "WhoWins\0GetCardType\0BlockCard\0n\0GetCard\0"
    "type\0IncreseScore\0IncreseScore1\0Sleep\0"
    "CreateNewGame\0numN_1\0numN_2\0ternN\0"
    "plOneScore\0plTwoScore"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GameInfoHS[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       5,  178, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x06 /* Public */,
       3,    0,  145,    2, 0x06 /* Public */,
       4,    0,  146,    2, 0x06 /* Public */,
       5,    0,  147,    2, 0x06 /* Public */,
       6,    0,  148,    2, 0x06 /* Public */,
       7,    0,  149,    2, 0x06 /* Public */,
       8,    0,  150,    2, 0x06 /* Public */,
       9,    0,  151,    2, 0x06 /* Public */,
      10,    0,  152,    2, 0x06 /* Public */,
      11,    0,  153,    2, 0x06 /* Public */,
      12,    0,  154,    2, 0x06 /* Public */,
      13,    0,  155,    2, 0x06 /* Public */,
      14,    0,  156,    2, 0x06 /* Public */,
      15,    0,  157,    2, 0x06 /* Public */,
      16,    0,  158,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    0,  159,    2, 0x0a /* Public */,
      18,    0,  160,    2, 0x0a /* Public */,
      19,    1,  161,    2, 0x0a /* Public */,
      21,    0,  164,    2, 0x0a /* Public */,
      22,    0,  165,    2, 0x0a /* Public */,
      23,    1,  166,    2, 0x0a /* Public */,
      25,    2,  169,    2, 0x0a /* Public */,
      27,    0,  174,    2, 0x0a /* Public */,
      28,    0,  175,    2, 0x0a /* Public */,
      29,    0,  176,    2, 0x0a /* Public */,
      30,    0,  177,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Bool, QMetaType::Int,   20,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      31, QMetaType::Int, 0x00495103,
      32, QMetaType::Int, 0x00495103,
      33, QMetaType::Bool, 0x00495103,
      34, QMetaType::Int, 0x00495103,
      35, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void GameInfoHS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GameInfoHS *_t = static_cast<GameInfoHS *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->numN_1Changed(); break;
        case 1: _t->numN_2Changed(); break;
        case 2: _t->ternNChanged(); break;
        case 3: _t->plOneScoreChanged(); break;
        case 4: _t->plTwoScoreChanged(); break;
        case 5: _t->readTwoNumbs(); break;
        case 6: _t->switchPlayer(); break;
        case 7: _t->blockGameCards(); break;
        case 8: _t->unblockGameCards(); break;
        case 9: _t->changeCardTypeGame(); break;
        case 10: _t->fieldTextChange(); break;
        case 11: _t->openChosenCard(); break;
        case 12: _t->closeTwoCards(); break;
        case 13: _t->newGameCreation(); break;
        case 14: _t->setTwoCardsCheckTrue(); break;
        case 15: { bool _r = _t->IsEndOfGame();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 16: { bool _r = _t->WaitSecCard();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: { bool _r = _t->PermisionForFlip((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 18: { int _r = _t->WhoWins();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 19: { int _r = _t->GetCardType();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 20: _t->BlockCard((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->GetCard((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->IncreseScore(); break;
        case 23: _t->IncreseScore1(); break;
        case 24: _t->Sleep(); break;
        case 25: _t->CreateNewGame(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::numN_1Changed)) {
                *result = 0;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::numN_2Changed)) {
                *result = 1;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::ternNChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::plOneScoreChanged)) {
                *result = 3;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::plTwoScoreChanged)) {
                *result = 4;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::readTwoNumbs)) {
                *result = 5;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::switchPlayer)) {
                *result = 6;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::blockGameCards)) {
                *result = 7;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::unblockGameCards)) {
                *result = 8;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::changeCardTypeGame)) {
                *result = 9;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::fieldTextChange)) {
                *result = 10;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::openChosenCard)) {
                *result = 11;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::closeTwoCards)) {
                *result = 12;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::newGameCreation)) {
                *result = 13;
            }
        }
        {
            typedef void (GameInfoHS::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfoHS::setTwoCardsCheckTrue)) {
                *result = 14;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        GameInfoHS *_t = static_cast<GameInfoHS *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->numN_1(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->numN_2(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->ternN(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->plOneScore(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->plTwoScore(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        GameInfoHS *_t = static_cast<GameInfoHS *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNumN_1(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setNumN_2(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setTernN(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setPlOneScore(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setPlTwoScore(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject GameInfoHS::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_GameInfoHS.data,
      qt_meta_data_GameInfoHS,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GameInfoHS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GameInfoHS::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GameInfoHS.stringdata0))
        return static_cast<void*>(const_cast< GameInfoHS*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int GameInfoHS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void GameInfoHS::numN_1Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void GameInfoHS::numN_2Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void GameInfoHS::ternNChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void GameInfoHS::plOneScoreChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void GameInfoHS::plTwoScoreChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void GameInfoHS::readTwoNumbs()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void GameInfoHS::switchPlayer()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void GameInfoHS::blockGameCards()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void GameInfoHS::unblockGameCards()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void GameInfoHS::changeCardTypeGame()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void GameInfoHS::fieldTextChange()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void GameInfoHS::openChosenCard()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void GameInfoHS::closeTwoCards()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void GameInfoHS::newGameCreation()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void GameInfoHS::setTwoCardsCheckTrue()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
