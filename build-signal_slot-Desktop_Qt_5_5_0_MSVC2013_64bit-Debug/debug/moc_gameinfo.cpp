/****************************************************************************
** Meta object code from reading C++ file 'gameinfo.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../ForZahar/CardGame_Ver3.3/gameinfo.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gameinfo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GameInfo_t {
    QByteArrayData data[45];
    char stringdata0[580];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GameInfo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GameInfo_t qt_meta_stringdata_GameInfo = {
    {
QT_MOC_LITERAL(0, 0, 8), // "GameInfo"
QT_MOC_LITERAL(1, 9, 13), // "numN_1Changed"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 13), // "numN_2Changed"
QT_MOC_LITERAL(4, 38, 12), // "ternNChanged"
QT_MOC_LITERAL(5, 51, 17), // "plOneScoreChanged"
QT_MOC_LITERAL(6, 69, 17), // "plTwoScoreChanged"
QT_MOC_LITERAL(7, 87, 12), // "readTwoNumbs"
QT_MOC_LITERAL(8, 100, 12), // "switchPlayer"
QT_MOC_LITERAL(9, 113, 14), // "blockGameCards"
QT_MOC_LITERAL(10, 128, 16), // "unblockGameCards"
QT_MOC_LITERAL(11, 145, 18), // "changeCardTypeGame"
QT_MOC_LITERAL(12, 164, 15), // "fieldTextChange"
QT_MOC_LITERAL(13, 180, 14), // "openChosenCard"
QT_MOC_LITERAL(14, 195, 13), // "closeTwoCards"
QT_MOC_LITERAL(15, 209, 9), // "endOfGame"
QT_MOC_LITERAL(16, 219, 15), // "newGameCreation"
QT_MOC_LITERAL(17, 235, 20), // "setTwoCardsCheckTrue"
QT_MOC_LITERAL(18, 256, 15), // "openWaitMessage"
QT_MOC_LITERAL(19, 272, 16), // "closeWaitMessage"
QT_MOC_LITERAL(20, 289, 11), // "IsEndOfGame"
QT_MOC_LITERAL(21, 301, 11), // "WaitSecCard"
QT_MOC_LITERAL(22, 313, 16), // "PermisionForFlip"
QT_MOC_LITERAL(23, 330, 3), // "num"
QT_MOC_LITERAL(24, 334, 7), // "WhoWins"
QT_MOC_LITERAL(25, 342, 11), // "GetCardType"
QT_MOC_LITERAL(26, 354, 17), // "GetChangeCardType"
QT_MOC_LITERAL(27, 372, 9), // "BlockCard"
QT_MOC_LITERAL(28, 382, 1), // "n"
QT_MOC_LITERAL(29, 384, 7), // "GetCard"
QT_MOC_LITERAL(30, 392, 4), // "type"
QT_MOC_LITERAL(31, 397, 12), // "IncreseScore"
QT_MOC_LITERAL(32, 410, 13), // "IncreseScore1"
QT_MOC_LITERAL(33, 424, 5), // "Sleep"
QT_MOC_LITERAL(34, 430, 19), // "SendDataToServerEnd"
QT_MOC_LITERAL(35, 450, 16), // "SendDataToServer"
QT_MOC_LITERAL(36, 467, 20), // "SendDataToServerTime"
QT_MOC_LITERAL(37, 488, 25), // "SendDataToServerDisconect"
QT_MOC_LITERAL(38, 514, 9), // "StartGame"
QT_MOC_LITERAL(39, 524, 13), // "CreateNewGame"
QT_MOC_LITERAL(40, 538, 6), // "numN_1"
QT_MOC_LITERAL(41, 545, 6), // "numN_2"
QT_MOC_LITERAL(42, 552, 5), // "ternN"
QT_MOC_LITERAL(43, 558, 10), // "plOneScore"
QT_MOC_LITERAL(44, 569, 10) // "plTwoScore"

    },
    "GameInfo\0numN_1Changed\0\0numN_2Changed\0"
    "ternNChanged\0plOneScoreChanged\0"
    "plTwoScoreChanged\0readTwoNumbs\0"
    "switchPlayer\0blockGameCards\0"
    "unblockGameCards\0changeCardTypeGame\0"
    "fieldTextChange\0openChosenCard\0"
    "closeTwoCards\0endOfGame\0newGameCreation\0"
    "setTwoCardsCheckTrue\0openWaitMessage\0"
    "closeWaitMessage\0IsEndOfGame\0WaitSecCard\0"
    "PermisionForFlip\0num\0WhoWins\0GetCardType\0"
    "GetChangeCardType\0BlockCard\0n\0GetCard\0"
    "type\0IncreseScore\0IncreseScore1\0Sleep\0"
    "SendDataToServerEnd\0SendDataToServer\0"
    "SendDataToServerTime\0SendDataToServerDisconect\0"
    "StartGame\0CreateNewGame\0numN_1\0numN_2\0"
    "ternN\0plOneScore\0plTwoScore"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GameInfo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       5,  232, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      18,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  189,    2, 0x06 /* Public */,
       3,    0,  190,    2, 0x06 /* Public */,
       4,    0,  191,    2, 0x06 /* Public */,
       5,    0,  192,    2, 0x06 /* Public */,
       6,    0,  193,    2, 0x06 /* Public */,
       7,    0,  194,    2, 0x06 /* Public */,
       8,    0,  195,    2, 0x06 /* Public */,
       9,    0,  196,    2, 0x06 /* Public */,
      10,    0,  197,    2, 0x06 /* Public */,
      11,    0,  198,    2, 0x06 /* Public */,
      12,    0,  199,    2, 0x06 /* Public */,
      13,    0,  200,    2, 0x06 /* Public */,
      14,    0,  201,    2, 0x06 /* Public */,
      15,    0,  202,    2, 0x06 /* Public */,
      16,    0,  203,    2, 0x06 /* Public */,
      17,    0,  204,    2, 0x06 /* Public */,
      18,    0,  205,    2, 0x06 /* Public */,
      19,    0,  206,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      20,    0,  207,    2, 0x0a /* Public */,
      21,    0,  208,    2, 0x0a /* Public */,
      22,    1,  209,    2, 0x0a /* Public */,
      24,    0,  212,    2, 0x0a /* Public */,
      25,    0,  213,    2, 0x0a /* Public */,
      26,    0,  214,    2, 0x0a /* Public */,
      27,    1,  215,    2, 0x0a /* Public */,
      29,    2,  218,    2, 0x0a /* Public */,
      31,    0,  223,    2, 0x0a /* Public */,
      32,    0,  224,    2, 0x0a /* Public */,
      33,    0,  225,    2, 0x0a /* Public */,
      34,    0,  226,    2, 0x0a /* Public */,
      35,    0,  227,    2, 0x0a /* Public */,
      36,    0,  228,    2, 0x0a /* Public */,
      37,    0,  229,    2, 0x0a /* Public */,
      38,    0,  230,    2, 0x0a /* Public */,
      39,    0,  231,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Bool, QMetaType::Int,   23,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Int,   28,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   30,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      40, QMetaType::Int, 0x00495103,
      41, QMetaType::Int, 0x00495103,
      42, QMetaType::Bool, 0x00495103,
      43, QMetaType::Int, 0x00495103,
      44, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void GameInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GameInfo *_t = static_cast<GameInfo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->numN_1Changed(); break;
        case 1: _t->numN_2Changed(); break;
        case 2: _t->ternNChanged(); break;
        case 3: _t->plOneScoreChanged(); break;
        case 4: _t->plTwoScoreChanged(); break;
        case 5: _t->readTwoNumbs(); break;
        case 6: _t->switchPlayer(); break;
        case 7: _t->blockGameCards(); break;
        case 8: _t->unblockGameCards(); break;
        case 9: _t->changeCardTypeGame(); break;
        case 10: _t->fieldTextChange(); break;
        case 11: _t->openChosenCard(); break;
        case 12: _t->closeTwoCards(); break;
        case 13: _t->endOfGame(); break;
        case 14: _t->newGameCreation(); break;
        case 15: _t->setTwoCardsCheckTrue(); break;
        case 16: _t->openWaitMessage(); break;
        case 17: _t->closeWaitMessage(); break;
        case 18: { bool _r = _t->IsEndOfGame();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 19: { bool _r = _t->WaitSecCard();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 20: { bool _r = _t->PermisionForFlip((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 21: { int _r = _t->WhoWins();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 22: { int _r = _t->GetCardType();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 23: { int _r = _t->GetChangeCardType();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 24: _t->BlockCard((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->GetCard((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: _t->IncreseScore(); break;
        case 27: _t->IncreseScore1(); break;
        case 28: _t->Sleep(); break;
        case 29: _t->SendDataToServerEnd(); break;
        case 30: _t->SendDataToServer(); break;
        case 31: _t->SendDataToServerTime(); break;
        case 32: _t->SendDataToServerDisconect(); break;
        case 33: _t->StartGame(); break;
        case 34: _t->CreateNewGame(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::numN_1Changed)) {
                *result = 0;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::numN_2Changed)) {
                *result = 1;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::ternNChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::plOneScoreChanged)) {
                *result = 3;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::plTwoScoreChanged)) {
                *result = 4;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::readTwoNumbs)) {
                *result = 5;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::switchPlayer)) {
                *result = 6;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::blockGameCards)) {
                *result = 7;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::unblockGameCards)) {
                *result = 8;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::changeCardTypeGame)) {
                *result = 9;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::fieldTextChange)) {
                *result = 10;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::openChosenCard)) {
                *result = 11;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::closeTwoCards)) {
                *result = 12;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::endOfGame)) {
                *result = 13;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::newGameCreation)) {
                *result = 14;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::setTwoCardsCheckTrue)) {
                *result = 15;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::openWaitMessage)) {
                *result = 16;
            }
        }
        {
            typedef void (GameInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GameInfo::closeWaitMessage)) {
                *result = 17;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        GameInfo *_t = static_cast<GameInfo *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->numN_1(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->numN_2(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->ternN(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->plOneScore(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->plTwoScore(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        GameInfo *_t = static_cast<GameInfo *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNumN_1(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setNumN_2(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setTernN(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setPlOneScore(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setPlTwoScore(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject GameInfo::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_GameInfo.data,
      qt_meta_data_GameInfo,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GameInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GameInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GameInfo.stringdata0))
        return static_cast<void*>(const_cast< GameInfo*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int GameInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 35;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void GameInfo::numN_1Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void GameInfo::numN_2Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void GameInfo::ternNChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void GameInfo::plOneScoreChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void GameInfo::plTwoScoreChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void GameInfo::readTwoNumbs()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void GameInfo::switchPlayer()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void GameInfo::blockGameCards()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void GameInfo::unblockGameCards()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void GameInfo::changeCardTypeGame()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void GameInfo::fieldTextChange()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void GameInfo::openChosenCard()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void GameInfo::closeTwoCards()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void GameInfo::endOfGame()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void GameInfo::newGameCreation()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void GameInfo::setTwoCardsCheckTrue()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void GameInfo::openWaitMessage()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void GameInfo::closeWaitMessage()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
