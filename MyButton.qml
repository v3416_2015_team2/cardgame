import QtQuick 2.4

Rectangle
{
    id: myButton

    property string text // текст кнопки

    color: "lightyellow"
    radius: 20

    // ширина и высота по умолчанию
    implicitWidth: buttonText.width + radius
    implicitHeight: buttonText.height + radius

    border
    {
        color: "silver"
        width: 4
    }

    Text
    {
        id: buttonText

        anchors.centerIn: parent
        text: myButton.text
    }

    signal clicked

    MouseArea
    {
        anchors.fill: parent
        onClicked: myButton.clicked()

        hoverEnabled: true
        onEntered: parent.border.color = "yellowgreen"
        onExited: parent.border.color = "silver"
    }
}
