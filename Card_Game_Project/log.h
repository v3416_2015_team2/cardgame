#pragma once
//#ifndef LOG_H
#define LOG_H

#if defined LOG_LIB
#define LOG_DLLSPEC Q_DECL_EXPORT
#else
#define LOG_DLLSPEC Q_DECL_IMPORT
#endif

#include <fstream>
#include <QString>

class LOG_DLLSPEC Log
{
private:
    std::ofstream _out;

    Log();
    ~Log();

    Log(const Log&);
    Log& operator=(const Log&);

public:
    static Log& instance()
    {
        static Log log;
        return log;
    }

    void print(QString const& message);
};
