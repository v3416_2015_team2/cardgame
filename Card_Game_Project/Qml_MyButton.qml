import QtQuick 2.4

Rectangle
{
    id: myButton

    property string text // С‚РµРєСЃС‚ РєРЅРѕРїРєРё

    color: "lightyellow"
    radius: 20

    // С€РёСЂРёРЅР° Рё РІС‹СЃРѕС‚Р° РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ
    implicitWidth: buttonText.width + radius
    implicitHeight: buttonText.height + radius

    border
    {
        color: "silver"
        width: 4
    }

    Text
    {
        id: buttonText

        anchors.centerIn: parent
        text: myButton.text
    }

    signal clicked

    MouseArea
    {
        anchors.fill: parent
        onClicked: myButton.clicked()

        hoverEnabled: true
        onEntered: parent.border.color = "yellowgreen"
        onExited: parent.border.color = "silver"
    }
}
