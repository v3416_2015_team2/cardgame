#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <time.h>
#include <QDebug>
//#include "receiver.h"


#include "gameinfo.h"
#include "gameinfoHS.h"
#include "cardinfo.h"





int main(int argc, char *argv[])
{
    srand (time(NULL));
    QGuiApplication app(argc, argv);
    qmlRegisterType<CardInfo>("Cards",1,0,"Card_Inf");
    qmlRegisterType<GameInfo>("Cards",1,0,"Game_Inf");
    qmlRegisterType<GameInfoHS>("Cards",1,0,"Game_Inf_HS");
    qmlRegisterType<PlayerInfo>("Cards",1,0,"Player_Inf");
    QQmlApplicationEngine engiMne;

    engiMne.load(QUrl(QStringLiteral("qrc:///main.qml")));
    return app.exec();
}
