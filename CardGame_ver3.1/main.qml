import QtQuick 2.2
import QtQuick.Window 2.1
import Cards 1.0


Window {
    id: test
    visible: true
    width: 720
        height: 1280

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("CLICK" )
            maintext.visible = false;
            game.visible = true;
        }
    }
    Text {
        id: maintext
        text: qsTr("Press me to START")
        anchors.centerIn: parent
    }

    Game
    {
        id: game
        visible: false
        anchors.fill: parent
        onExitToMenu:
        {
            //console.log("Received in QML from C++iii: " )
            game.visible = false;
            test.visible = true;
        }
    }

}
