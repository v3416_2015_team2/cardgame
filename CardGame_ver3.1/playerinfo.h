#ifndef PLAYERINFO_H
#define PLAYERINFO_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <random>
#include <vector>
using namespace std;

class PlayerInfo : public QQuickItem
{
    Q_OBJECT
private:
    //Q_PROPERTY(int scorN READ scorN WRITE setScorN NOTIFY scorNChanged)
    int scoreNum;
    int playerNum;

public:
    PlayerInfo() {};
    PlayerInfo(int p);
    int scorN();
    void setScorN(int sc);
signals:
    void scorNChanged();


public slots:
    void addScore();
};

#endif // PLAYERINFO_H
