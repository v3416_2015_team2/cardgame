#ifndef CHECKLOAD_H
#define CHECKLOAD_H

#include <QObject>

class CheckLoad : public QObject
{
    Q_OBJECT
public:
    explicit CheckLoad(QObject *parent = 0);
    ~CheckLoad();

    bool IfError() const;

signals:

public slots:
    void ComponentStatusChanged(QObject* result, const QUrl&);

private:
    bool look_error;
};

#endif // CHECKLOAD_H
