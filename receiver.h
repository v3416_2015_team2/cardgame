#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>
#include <vector>
#include <random>
#include <string>
#include <QString>

using namespace std;
class Receiver : public QObject
{
    Q_OBJECT
private:
    vector <int> allCard;
public:
    explicit Receiver(QObject *parent = 0);


signals:
    void sendToQml(int count);
    //void sendCardTop(QString count);
    void sendBoolNum(bool count);
    void sendCharTop(char count);

public slots:
    void receiveFromQml(int count);
    QString createCard();

};

#endif // RECEIVER_H
