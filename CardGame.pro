TEMPLATE = app

QT += qml quick core declarative

SOURCES += main.cpp \
    checkload.cpp \
    cardinfo.cpp \
    gameinfo.cpp \
    playerinfo.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    checkload.h \
    cardinfo.h \
    gameinfo.h \
    playerinfo.h
