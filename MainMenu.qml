import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

Item
{
    signal start
    signal options
    signal howtoplay
    signal startnetwork

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 10

        Text
        {
            text: qsTr("Find a couple!");

            font
            {
                bold: true
                family: "Verdana"
                pointSize: 14
            }

            Layout.alignment: Qt.AlignHCenter
        }

        Item
        {
            Layout.fillWidth: true
            Layout.fillHeight: true

            SingleBlock
            {
                color: "yellow"
                figure: [
                    [0,0,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,1,1,0,0,0,0],
                    [0,0,1,0,0,0,0,0,1,0,0,1,1,0,0,1,0,1,0,1,0,0,0],
                    [0,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,0],
                    [0,0,1,0,0,0,0,0,1,0,0,1,0,0,1,1,0,1,0,1,0,0,0],
                    [0,0,1,0,0,0,0,1,1,1,0,1,0,0,0,1,0,1,1,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0],
                    [1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
                    [1,0,0,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,0,0],
                    [1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,1,0],
                    [1,0,0,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,0,0],
                    [1,1,1,0,1,1,1,0,0,1,1,0,1,0,0,0,1,1,1,0,1,1,1]
                ]

                anchors.centerIn: parent
            }
        }

        MyButton
        {
            text: qsTr("New game")

            Layout.fillWidth: true

            onClicked:
            {
                start();
            }
        }

        MyButton
        {
            text: qsTr("New Network Game")

            Layout.fillWidth: true

            onClicked:
            {
                startnetwork();
            }
        }

        MyButton
        {
            text: qsTr("Options")

            Layout.fillWidth: true

            onClicked:
            {
                options();
            }
        }

        MyButton
        {
            text: qsTr("How to play")

            Layout.fillWidth: true

            onClicked:
            {
                howtoplay();
            }
        }

        MyButton
        {
            text: qsTr("Quit game")

            Layout.fillWidth: true

            onClicked:
            {
                Qt.quit();
            }
        }
    }
}



