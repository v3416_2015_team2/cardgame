#ifndef MSERVER_H
#define MSERVER_H

#include <QMainWindow>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QByteArray>
#include <QDataStream>
#include <QString>

class MyServer : public QMainWindow
{
    Q_OBJECT
private:
    QTcpServer *server; // СѓРєР°Р·Р°С‚РµР»СЊ РЅР° СЃРµСЂРІРµСЂ
    QList<QTcpSocket *> sockets; // РїРѕР»СѓС‡Р°С‚РµР»Рё РґР°РЅРЅС‹С…
    QTcpSocket *firstSocket;
    QTcpSocket *secondSocket;// РІРµС‰Р°С‚РµР»СЊ
    int ammOfSocets;
    QByteArray firPlData;
    QByteArray secPlData;
public:
    explicit MyServer(QMainWindow *parent = 0); // РєРѕРЅСЃС‚СЂСѓРєС‚РѕСЂ
signals:
public slots:
    void getNewConnection(); // РѕР±СЂР°Р±РѕС‚С‡РёРє РІС…РѕРґСЏС‰РµРіРѕ РїРѕРґРєР»СЋС‡РµРЅРёСЏ
    void doRead();
    void doDiscon();
    void doRead_2();
    void doDiscon_2();
    void doSendDataToFir();
    void doSendDataToSec();

};

#endif // MSERVER_H
