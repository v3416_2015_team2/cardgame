import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import Cards 1.0
Item {

    id: field
    width: 480
    height: 800
    property int iTest:0
    property bool prevTern:true
    property bool boolForBloc:game_info.ternN
    property bool twoCardsCheck:false
    property string cardName: "card_0"
    property variant items:[card_0,card_1,card_2,card_3,card_4,card_5,card_6,card_7,card_8,card_9,
                            card_10,card_11,card_12,card_13,card_14,card_15,card_16,card_17,card_18,card_19,
                            card_20,card_21,card_22,card_23,card_24,card_25,card_26,card_27,card_28,card_29]
    //signal exitToMenu
    signal testSignal
    signal closeChoseCards
    signal blockCards

    Connections {
        target: field
        onTestSignal: {
            for ( iTest=0; iTest<30;iTest++)
            {
                //console.log(itemsBlockBool[iTest])
                if (!game_info.PermisionForFlip(iTest))
                items[iTest].runFlippe()

            }
        }
    }
    Connections {
        target: field
        onCloseChoseCards: {
            field.blockCards()
            twoCardsCheck=true
        }
    }
    Connections {
        target: field
        onBlockCards: {
            for ( iTest=0; iTest<30;iTest++)
            {
                items[iTest].stopFlippe()
            }
        }
    }
    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            if (twoCardsCheck)
            {

                twoCardsCheck=false


                if (prevTern == game_info.ternN)
                {
                    console.log("Cards Was Blocked")
                    game_info.BlockCard(game.numFir)
                    game_info.BlockCard(game.numSec)
                    if(!game_info.ternN)
                        plTwo.addScore()
                    else
                        plOne.addScore()
                }
                else
                {
                items[game.numFir].closeCard()
                items[game.numSec].closeCard()
                }
                prevTern = game_info.ternN

                textMes.text = qsTr("SecondPlayerTurn")
                textScore.text = qsTr(""+plTwo.scorN)
                if (game_info.ternN)
                {
                   textMes.text= qsTr("FirstPlayerTurn")
                   textScore.text = qsTr(""+plOne.scorN)
                }

                field.testSignal()
            }
        }
    }
    Text {
        id: textMes
        text: qsTr("FirstPlayerTurn")
        y:650
        x:parent.width/2-15
    }
    Text {
        id: textScore
        text: qsTr(""+plOne.scorN)
        y:650
        x:parent.width/2+100
    }

    Row
    {
        //anchors.centerIn: parent
        spacing: 30
        Card
        {
            id:card_0
            y: 10
            cardType: game_info.GetCardType()
            cardNomer:0
        }

        Card
        {
            id:card_1
            y: 10
            cardType: game_info.GetCardType()
            cardNomer:1
        }

        Card
        {
            id:card_2
            y: 10
            cardType: game_info.GetCardType()
            cardNomer:2
        }

        Card
        {
            id:card_3
            y: 10
            cardType: game_info.GetCardType()
            cardNomer:3
        }

        Card
        {
            id:card_4
            y: 10
            cardType: game_info.GetCardType()
            cardNomer:4
        }
    }

    Row
    {
        spacing: 30

        Card
        {
            id:card_5
            y: 110
            cardType: game_info.GetCardType()
            cardNomer:5
        }

        Card
        {
            id:card_6
            y: 110
            cardType: game_info.GetCardType()
            cardNomer:6
        }

        Card
        {
            id:card_7
            y: 110
            cardType: game_info.GetCardType()
            cardNomer:7
        }

        Card
        {
            id:card_8
            y: 110
            cardType: game_info.GetCardType()
            cardNomer:8
        }

        Card
        {
            id:card_9
            y: 110
            cardType: game_info.GetCardType()
            cardNomer:9
        }
    }

    Row
    {
        spacing: 30

        Card
        {
            id:card_10
            y: 210
            cardType: game_info.GetCardType()
            cardNomer:10
        }

        Card
        {
            id:card_11
            y: 210
            cardType: game_info.GetCardType()
            cardNomer:11
        }

        Card
        {
            id:card_12
            y: 210
            cardType: game_info.GetCardType()
            cardNomer:12
        }

        Card
        {
            id:card_13
            y: 210
            cardType: game_info.GetCardType()
            cardNomer:13
        }

        Card
        {
            id:card_14
            y: 210
            cardType: game_info.GetCardType()
            cardNomer:14
        }
    }

    Row
    {
        spacing: 30

        Card
        {
            id:card_15
            y: 310
            cardType: game_info.GetCardType()
            cardNomer:15
        }

        Card
        {
            id:card_16
            y: 310
            cardType: game_info.GetCardType()
            cardNomer:16
        }

        Card
        {
            id:card_17
            y: 310
            cardType: game_info.GetCardType()
            cardNomer:17
        }

        Card
        {
            id:card_18
            y: 310
            cardType: game_info.GetCardType()
            cardNomer:18
        }

        Card
        {
            id:card_19
            y: 310
            cardType: game_info.GetCardType()
            cardNomer:19
        }
    }

    Row
    {
        spacing: 30

        Card
        {
            id:card_20
            y: 410
            cardType: game_info.GetCardType()
            cardNomer:20
        }

        Card
        {
            id:card_21
            y: 410
            cardType: game_info.GetCardType()
            cardNomer:21
        }

        Card
        {
            id:card_22
            y: 410
            cardType: game_info.GetCardType()
            cardNomer:22
        }

        Card
        {
            id:card_23
            y: 410
            cardType: game_info.GetCardType()
            cardNomer:23
        }

        Card
        {
            id:card_24
            y: 410
            cardType: game_info.GetCardType()
            cardNomer:24
        }
    }

    Row
    {
        spacing: 30

        Card
        {
            id:card_25
            y: 510
            cardType: game_info.GetCardType()
            cardNomer:25
        }

        Card
        {
            id:card_26
            y: 510
            cardType: game_info.GetCardType()
            cardNomer:26
        }

        Card
        {
            id:card_27
            y: 510
            cardType: game_info.GetCardType()
            cardNomer:27
        }

        Card
        {
            id:card_28
            y: 510
            cardType: game_info.GetCardType()
            cardNomer:28
        }

        Card
        {
            id:card_29
            y: 510
            cardType: game_info.GetCardType()
            cardNomer:29
        }
    }
    Button
    {
        text: qsTr("Back to main menu")
        x: 350
        y: 700
        onClicked: exitToMenu()
    }
    Button
    {
        text: qsTr("TestButton")
        x: 240
        y: 750
        onClicked: testSignal()
    }



}

