import Shapes 1.0
import QtQuick 2.2
import QtQuick.Window 2.1
import Cards 1.0


Window {
    id: test
    visible: true
    width: 480
    height: 800


    Connections {
        target: receiver
        onSendToQml: {
            console.log("Received in QML from C++: " + count)
        }
    }


    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("CLICK" )
            visible = false;
            game.visible = true;
        }
    }
    Text {
        text: qsTr("Press me to START")
        anchors.centerIn: parent
    }
    QMLCard
    {
        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        height : parent.height /2-6
    }
    Game
    {
        id: game
        visible: false
        anchors.fill: parent
        onExitToMenu:
        {
            //console.log("Received in QML from C++iii: " )
            game.visible = false;
            test.visible = true;
        }
    }

}
