#ifndef CARDINFO_H
#define CARDINFO_H

#include <QQuickItem>
#include <QString>
#include <QDebug>
#include <random>

class CardInfo : public QQuickItem
{
    Q_OBJECT
private:
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int numN READ numN WRITE setNumN NOTIFY numNChanged)
    QString BotName;
    int num;
    int tesType;
    bool flippeAble;
public:
    CardInfo(QQuickItem* parent = 0);
    QString GetName();
    int numN();
    void setNumN(int t);
    int type();
    void setType(int t);
signals:
    void typeChanged();
    void numNChanged();
public slots:
    QString GetCardBack();
};

#endif // CARDINFO_H
